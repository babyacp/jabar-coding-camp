// soal 1

//output = saya senang belajar JAVASCRIPT

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var upper = kedua.substr(8, 10).toUpperCase();  // mengubah javascript menjadi JAVASCRIPT

console.log(pertama.substr(0, 4).concat(" ").concat(pertama.substr(12, 6)).concat(" ").concat(kedua.substr(0, 7).concat(" ").concat(upper)));

//-----------------------------------------------------------

//soal 2

//output = 24 (integer)

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var kataPertamaNumber = Number(kataPertama);  // kataPertama menjadi integer --> "10" menjadi 10
var kataKeduaNumber = Number(kataKedua);  // kataKedua menjadi integer --> "2" menjadi 2
var kataKetigaNumber = Number(kataKetiga);  // kataKetiga menjadi integer --> "4" menjadi 4
var kataKeempatNumber = Number(kataKeempat);  // kataKeempat menjadi integer --> "6" menjadi 6

var hasiloutput = (kataPertamaNumber % kataKeduaNumber) + (kataKetigaNumber * kataKeempatNumber);

console.log(hasiloutput);

//-----------------------------------------------------------

//soal 3

//output:
//Kata Pertama: wah
//Kata Kedua: javascript
//Kata Ketiga: itu
//Kata Keempat: keren
//Kata Kelima: sekali

var kalimat = 'wah javascript itu keren sekali';

var kataPertamaKalimat = kalimat.substring(0, 3);
var kataKeduaKalimat = kalimat.substring(4, 14);
var kataKetigaKalimat = kalimat.substring(15, 18);
var kataKeempatKalimat = kalimat.substring(19, 24);
var kataKelimaKalimat = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertamaKalimat);
console.log("Kata Kedua: " + kataKeduaKalimat);
console.log("Kata Ketiga: " + kataKetigaKalimat);
console.log("Kata Keempat: " + kataKeempatKalimat);
console.log("Kata Kelima: " + kataKelimaKalimat);
