// soal 1

// di index.js
var readBooks = require('./callback.js')

let time = 10000;

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 5000 }
]

// Tulis code untuk memanggil function readBooks di sini

readBooks(time, books[0], function (sisaWaktu) {
  readBooks(sisaWaktu, books[1], function (sisaWaktu) {
    readBooks(sisaWaktu, books[2], function (sisaWaktu) {
      readBooks(sisaWaktu, books[3], function (sisaWaktu) {
      })
    })
  })
})
