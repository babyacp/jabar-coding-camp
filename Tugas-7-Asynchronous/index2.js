// soal 2

var readBooksPromise = require('./promise.js')

let time = 10000;

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 }
]

readBooksPromise(time, books[0])
  .then((sisaWaktu) => {
    return readBooksPromise(sisaWaktu, books[1]);
  })
  .then((sisaWaktu) => {
    return readBooksPromise(sisaWaktu, books[2]);
  })
  .catch((sisaWaktu) => {
    return readBooksPromise(sisaWaktu, books[3]);
  })
