// soal 1

var nilai = 30;
var indeks1 = "A";
var indeks2 = "B";
var indeks3 = "C";
var indeks4 = "D";
var indeks5 = "E";

if (nilai >= 85) {
  console.log("Nilai: " + nilai + " ; " + "Indeks: " + indeks1);
} else if (nilai >= 75 && nilai < 85) {
  console.log("Nilai: " + nilai + " ; " + "Indeks: " + indeks2);
} else if (nilai >= 65 && nilai < 75) {
  console.log("Nilai: " + nilai + " ; " + "Indeks: " + indeks3);
} else if (nilai >= 55 && nilai < 65) {
  console.log("Nilai: " + nilai + " ; " + "Indeks: " + indeks4);
} else {
  console.log("Nilai: " + nilai + " ; " + "Indeks: " + indeks5);
}

// -------------------------------------

// soal 2

var tanggal = 26;
var bulan = 8;
var tahun = 1997;

switch (bulan) {
  case 1:
    bulan = "Januari"; break;
  case 2:
    bulan = "Februari"; break;
  case 3:
    bulan = "Maret"; break;
  case 4:
    bulan = "April"; break;
  case 5:
    bulan = "Mei"; break;
  case 6:
    bulan = "Juni"; break;
  case 7:
    bulan = "Juli"; break;
  case 8:
    bulan = "Agustus"; break;
  case 9:
    bulan = "Semptember"; break;
  case 10:
    bulan = "Oktober"; break;
  case 11:
    bulan = "November"; break;
  default:
    bulan = "Desember";
}

console.log(tanggal + " " + bulan + " " + tahun);

// -------------------------------------

// tugas 3

// n = 3
var n1 = 3;
var string1 = "";

for (var i = 1; i <= n1; i++) {
  for (var j = 0; j < i; j++) {
    string1 = string1 + "#";
  }
  string1 += "\n"
}
console.log(string1);

// n = 7
var n2 = 7;
var string2 = "";

for (var i = 1; i <= n2; i++) {
  for (var j = 0; j < i; j++) {
    string2 = string2 + "#";
  }
  string2 += "\n"
}
console.log(string2);

// -------------------------------------

// soal 4

var word1 = "I love programming";
var word2 = "I love Javascript";
var word3 = "I love VueJS";
var text = "";
var tempNum = 0;

var m = 10; // m dapat diganti dengan nilai berapapun

for (var i = 0; i < m; i++) {
  tempNum++;
  if (tempNum === 1) {
    console.log(i + 1 + " - " + word1);
  } else if (tempNum === 2) {
    console.log(i + 1 + " - " + word2);
  } else if (tempNum === 3) {
    console.log(i + 1 + " - " + word3);
    if (i % 4 === 2) {
      console.log("===");
    } else if (i % 4 === 1) {
      console.log("======");
    } else if (i % 4 === 0) {
      console.log("=========");
    } else {
      console.log("===");
    }

    tempNum = 0;
  }
}

console.log(text);