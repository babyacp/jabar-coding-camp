// soal 1

var tanggal = 31
var bulan = 12
var tahun = 2020

function next_date(tanggal, bulan, tahun) {
  var days = 31
  var months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "November", "Desember"]
  var result

  if (tanggal <= 31 && bulan <= 12) {
    for (var i = 0; i < months.length; i++) {
      if (i === bulan) {
        if (i % 2 === 0 && i !== 2) {
          days = days - 1
          tanggal = tanggal + 1

          if (tanggal <= days) {
            result = "" + tanggal + " " + months[i] + " " + tahun
          } else {
            if (i === 12) {
              tanggal = 1
              tahun = tahun + 1
              result = "" + tanggal + " " + months[1] + " " + tahun
            } else {
              tanggal = 1
              result = "" + tanggal + " " + months[i + 1] + " " + tahun
            }
          }
        } else if (i % 2 !== 0) {
          tanggal = tanggal + 1

          if (tanggal <= days) {
            result = "" + tanggal + " " + months[i] + " " + tahun
          } else {
            tanggal = 1
            result = "" + tanggal + " " + months[i + 1] + " " + tahun
          }
        } else {
          if (tahun % 4 === 0) {
            days = days - 2
            tanggal = tanggal + 1

            if (tanggal <= days) {
              result = "" + tanggal + " " + months[i] + " " + tahun
            } else {
              tanggal = 1
              result = "" + tanggal + " " + months[i + 1] + " " + tahun
            }
          } else {
            days = days - 3
            tanggal = tanggal + 1

            if (tanggal <= days) {
              result = "" + tanggal + " " + months[i] + " " + tahun
            } else {
              tanggal = 1
              result = "" + tanggal + " " + months[i + 1] + " " + tahun
            }
          }
        }
      }
    }
  } else {
    result = "invalid tanggal atau bulan"
  }
  return result
}

console.log(next_date(tanggal, bulan, tahun));

// soal 2
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

function jumlah_kata(kalimat) {
  var result = 0;
  var tampungArray = [];
  var tampungString = "";

  for (i = 0; i <= kalimat.length; i++) {
    if (kalimat[i] != " ") {
      tampungString = tampungString + kalimat[i]
    } else {
      if (tampungString.length !== 0) {
        tampungArray.push(tampungString);
        tampungString = "";
      }
    }
  }
  result = tampungArray.length
  return result;
}

console.log(jumlah_kata(kalimat_1));
