// soal 1

let daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

function bubbleSort(array) {
  var done = false;
  while (!done) { // while it's true
    done = true;
    for (var i = 1; i < array.length; i += 1) {
      if (array[i - 1] > array[i]) {
        done = false;
        var tmp = array[i - 1];
        array[i - 1] = array[i];
        array[i] = tmp;
      }
    }
  }
  return array;
}

sortedDaftarHewan = (bubbleSort(daftarHewan));

for (idx in sortedDaftarHewan) {
  console.log(sortedDaftarHewan[idx])
}

// soal 2

let data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

function introduce(data) {
  var name = data.name;
  var age = data.age;
  var address = data.address;
  var hobby = data.hobby;
  return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + " dan saya punya hobby yaitu " + hobby + "!";
}


console.log(introduce(data));

// soal 3

function hitung_huruf_vokal(str) {
  var count = 0;
  for (var i = 0; i < str.length; i++) {
    if (str[i].toLowerCase() == 'a' || str[i].toLowerCase() == 'i' || str[i].toLowerCase() == 'o' || str[i].toLowerCase() == 'e' || str[i].toLowerCase() == 'u') {
      count += 1;
    }
  }
  return count;
}

hitung_1 = hitung_huruf_vokal("Muhammad");
hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2);

// soal 4

function hitung(num) {
  return num * 2 - 2;
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(4)) // 6
console.log(hitung(5)) // 8
console.log(hitung(6)) // 10