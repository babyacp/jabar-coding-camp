// soal 1

// luas persegi panjang
let luas = (p, l) => p * l;

console.log(luas(2, 3));  // 6

// keliling persegi panjang
let keliling = (p, l) => 2 * (p + l);

console.log(keliling(2, 3));

//---------------------------------------

// soal 2

const newFunction = (firstName, lastName) => {
  const obj = {
    firstName,
    lastName,
    fullName: () => {
      return `${obj.firstName} ${obj.lastName}`
    }
  }
  return obj.fullName()
};
console.log(newFunction("Wiliam", "Imoh"))

//---------------------------------------

// soal 3

const newObject = {
  firstName: "Muhammad", lastName: "Iqbal Mubarok", address: "Jalan Ranamanyar", hobby: "playing football"
};

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

//---------------------------------------

// soal 4

const west = ["Will", "Chris", "Sam", "Holly",];
const east = ["Gill", "Brian", "Noel", "Maggie"];

function gabungArray(p1, p2) {
  let result = [];
  result = [...p1, ...p2]
  return result;
}
console.log(gabungArray(west, east));

//---------------------------------------

// soal 5

const planet = "earth";
const view = "glass";

console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`);
